package main

import (
	"fmt"
	"os/exec"
	"strings"
	"regexp"
	"path/filepath"
	"os"
	"io/ioutil"
)

var templateNamespaces = make([]string, 0, 0)
var namespaces = make([]string, 0, 0)
var files = make([]string, 0, 0)

func visitSoy(path string, f os.FileInfo, err error) error {
	if !f.IsDir() {
		filename := f.Name()
		if strings.HasSuffix(filename, ".soy") {
			cssPath := strings.Replace(path, ".soy", ".css", 1)
			templateNamespace := getTemplateNamespace(cssPath)
			namespace := getNamespace(path)
			if templateNamespace != "" && namespace != "" {
				files = append(files, path)
				namespaces = append(namespaces, namespace)
				templateNamespaces = append(templateNamespaces, templateNamespace)
			}
		}
	}
	return nil	
}

func getTemplateNamespace(cssPath string) string {
	provideRe := regexp.MustCompile("@provide\\s+['\"](.+)['\"]")
	bytesContent, err := ioutil.ReadFile(cssPath)
	if err != nil {
		fmt.Println("Could not read" + cssPath)
		os.Exit(1)
	}
	content := string(bytesContent)
	lines := strings.Split(content, "\n")
	for _, line := range lines {
		matches := provideRe.FindStringSubmatch(line)
		if len(matches) == 2 {
			return matches[1]
		}
	}
	return ""
}
func getNamespace(path string) string {
	re := regexp.MustCompile("\\{\\s*namespace\\s*((\\w+|\\.)+)\\}")	
	bytesContent, err := ioutil.ReadFile(path)
	if err != nil {
		fmt.Println("Could not read" + path)
		os.Exit(1)
	}
	content := string(bytesContent)
	lines := strings.Split(content, "\n")
	for _, line := range lines {
		matches := re.FindStringSubmatch(line)
		if len(matches) >0 {
			return matches[1]
		}
	}
	return ""
}

func main() {
	err := filepath.Walk(".", visitSoy)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	jarPath := "/home/andrey/Programming/go_test/closure-templates/build/SoyToGoSrcCompilerExperimental.jar"
	args := []string{"-jar", jarPath, "--codeStyle", "concat"}
	for _, file := range files {
		args = append(args, file)
	}
	cmd := exec.Command("java", args...)
	output, err := cmd.Output()
	if err != nil {
		fmt.Println(err)
		fmt.Println("Soy files might be missing")
		os.Exit(1)
	}
	content := string(output)
	packages := strings.Split(content, "// -----------------------------------------------------------------------------")
	packages = packages[1:]
	fmt.Println(`package main
import "closure/template/soyutil"

const DEFAULT_BUFFER_SIZE_IN_BYTES = 8192
`)
	templateFuncs := []string{}
	for i, p := range packages {
		packageContent := ""
		if (i != len(packages)-1) {
			packageContent = removeExtraLines(p, 5, 6)
		} else {
			packageContent = removeExtraLines(p, 4, 1)
		}
		packageContent = addNamespace(packageContent, namespaces[i])
		packageContent = replaceFuncCalls(packageContent, namespaces[i])
		packageContent = replaceGetData(packageContent)
		packages[i] = packageContent
		templateFuncs = append(templateFuncs, lastFunc(packageContent))
		fmt.Println(packageContent)
	}
	fmt.Println(`
var Templates = map[string]func(soyutil.SoyMapData)string {`)
	for i, funcName := range templateFuncs {
		fmt.Println(`"`+templateNamespaces[i]+`": `+ funcName + ",")
	}
	fmt.Println(`}`)
}

func removeExtraLines(content string, top int, bottom int) string{
	lines := strings.Split(content, "\n")
	return strings.Join(lines[top:len(lines)-bottom], "\n")
}

func addNamespace(content, namespace string) string {
	modifiedNamespace := strings.Replace(namespace, ".", "_", -1)
	modifiedNamespace += "_"
	re := regexp.MustCompile("(^|\\n)func\\s")
	return re.ReplaceAllString(content, "func "+modifiedNamespace)
}

func replaceFuncCalls(content, namespace string) string {
	replaceNamespace := strings.Replace(strings.ToLower(namespace), ".", "\\$", -1)
	re := regexp.MustCompile("(?i)"+replaceNamespace + "\\$\\w")
	matchedStrings := re.FindAllString(content, -1)
	if (len(matchedStrings) > 0) {
		newContent := content
		modifiedNamespace := strings.Replace(namespace, ".", "_", -1)
		modifiedNamespace += "_"
		for _, str := range matchedStrings {
			letter := string(str[len(str)-1]) + ""
			letter = strings.ToUpper(letter)
			newContent = strings.Replace(newContent, str, modifiedNamespace + letter, -1)
		}
		return newContent
	}
	return content
}

func lastFunc(content string) string {
	lines := strings.Split(content, "\n")
	re := regexp.MustCompile("^func\\s+(\\w+)\\(")
	for i:=len(lines)-1; i>=0; i-- {
		matches := re.FindStringSubmatch(lines[i])
		if (len(matches) > 0){
			return matches[1]
		}
	}
	return ""
}

func replaceGetData(content string) string {
	re := regexp.MustCompile("(soyutil\\.GetData\\([\\w|\\s|,|\"]+\\))([\\s|$])")
	return re.ReplaceAllString(content, "$1.String()$2")
}
